---
title: 'ITT1 Project'
subtitle: 'Exercises'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'ITT3 Network Automation, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/19a-itt3-network-automation/19A_ITT3_Network_Automation_weekly_plans.pdf)



