---
Week: 43
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 43 Network Automation

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Installing and setting up PyeZ for Junos devices.  
* CLI in PyeZ

### Topics to be covered

* Python Virtual Environment on Linux.
* Installing and setting up PyeZ for Junos devices.  
* CLI in PyeZ.  

## Deliverables

* Assignment TBD

## Schedule

* Thursdays and Friday week 43 to week 50

## Hands-on time

* See deliverables.

## Comments

TBD

## Sources

* TBD  

## White Board

TBD

