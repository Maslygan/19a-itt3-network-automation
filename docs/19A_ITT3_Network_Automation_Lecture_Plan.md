---
title: '19A ITT3 Network Automation'
subtitle: 'Lecture plan'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: PDA
* Title of the course, module or project and ECTS: Network Automation, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Lecturer |  Week | Content |
| :---: | :---: | :--- |
|  PDA | 43 | Introduction to PyeZ, tools and online resources |
|  PDA | 44 | PyeZ |
|  PDA | 45 | Paramiko |
|  PDA | 46 | Netmiko |
|  PDA | 47 | Ansibel  |
|  PDA | 48 | Ansibel |
|  PDA | 49 | Database or Automatik Logging/Logserver |
|  PDA | 50 | Exam |

# General info about the course, module or project

The purpose of the course is to gain insight into automating repetitive networking administrtion tasks.

## The student’s learning outcome

At the end of the course, the student will be able to automate repetitive networking administrtion tasks.  
The automation will mainly be through Python scripts/programmes.

## Content

Pleasse see weekly plans.

## Method

Pleasse see weekly plans.

## Equipment

LapTop, VMware Workstation, UCL Networking Lab equipment.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
The subject includes 1 Obligatory Learnig Activity - OLA.

See exam catalogue for details on Obligatory Learnig Activity - OLA.

## Other general information
None at this time.
